<?php
/**
 * WordPress Landing Page Config File
 * Template Name:    Demo Template
 * @package    WordPress Landing Pages
 * @author    Inbound Now
 *
 * This is a demo template for developers and designers to use as a reference for building landing page templates
 * for Wordpress Landing Pages Plugin http://wordpress.org/plugins/landing-pages/
 *
 * As of September 20015 we've begun using Advanced Custom Fields to design our Landing Page Template's dynamic fields:
 * http://www.advancedcustomfields.com/
 *
 */

/* get the name of the template folder */
$key = basename(dirname(__FILE__));

/* discover the absolute path of where this template is located. Core templates are loacted in /wp-content/plugins/landing-pages/templates/ while custom templates belong in /wp-content/uploads/landing-pages/tempaltes/ */
$path = (preg_match("/uploads/", dirname(__FILE__))) ? LANDINGPAGES_UPLOADS_URLPATH . $key .'/' : LANDINGPAGES_URLPATH.'templates/'.$key.'/'; // This defines the path to your template folder. /wp-content/uploads/landing-pages/templates by default


/* This is where we setup our template meta data */
$lp_data[$key]['info'] = array(
	'data_type' => "acf4", 												/* tell landing pages that this data represents a landing page template powered by ACF */
	'version' => "2.0.0", 												/* lets give our template a version number */
	'label' => __( 'Demo','landing-pages'), 							/* Let's give our template a nice name */
	'category' => 'Demo', 												/* you can categorize your landing pages by adding comma separated keywords */
	'uber' => 'localhost', 	/* a link to a third party demo page if applicable */
	'description'	=> __( 'uber theme' , 'uber' ) /* template description here! */
);

/* now setup ACF field definitions */
if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_55de1ff86df89',
		'title' => __('Demo Landing Page','landing-pages'),
		'fields' => array (
			array (
				'key' => 'field_55de20a76221c',
				'label' => __('Conversion area','landing-pages'),
				'name' => 'conversion-area-content',
				'type' => 'wysiwyg',
				'instructions' => __('Input your call to action / Inbound form shortcodes here. ','landing-pages'),
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'landing-page',
				),
				array (
					'param' => 'template_id',
					'operator' => '==',
					'value' => $key,
				)
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
		'options' => array(),
	));

endif;